<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exception\HttpResponseException;
use App\User;
use App\Users_detail;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * Handle a login request to the application.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        try {
            $this->validate($request, [
                'email' => 'required|email|max:255',
                'password' => 'required',
            ]);
        } catch (ValidationException $e) {
            return $e->getResponse();
        }

        try {
            // Attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt(
                $this->getCredentials($request)
            )) {
                return $this->onUnauthorized();
            }
        } catch (JWTException $e) {
            // Something went wrong whilst attempting to encode the token
            return $this->onJwtGenerationError();
        }

        // All good so return the token
        return $this->onAuthorized($token);
    }

    /**
     * What response should be returned on invalid credentials.
     *
     * @return JsonResponse
     */
    protected function onUnauthorized()
    {
        return new JsonResponse([
            'message' => 'invalid_credentials'
        ], Response::HTTP_UNAUTHORIZED);
    }

    /**
     * What response should be returned on error while generate JWT.
     *
     * @return JsonResponse
     */
    protected function onJwtGenerationError()
    {
        return new JsonResponse([
            'message' => 'could_not_create_token'
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * What response should be returned on authorized.
     *
     * @return JsonResponse
     */
    protected function onAuthorized($token)
    {
        return new JsonResponse([
            'message' => 'token_generated',
            'data' => [
                'token' => $token,
            ]
        ]);
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        return $request->only('email', 'password');
    }

    /**
     * Invalidate a token.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteInvalidate()
    {
        $token = JWTAuth::parseToken();

        $token->invalidate();

        return new JsonResponse(['message' => 'token_invalidated']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\Response
     */
    public function patchRefresh()
    {
        $token = JWTAuth::parseToken();

        $newToken = $token->refresh();

        return new JsonResponse([
            'message' => 'token_refreshed',
            'data' => [
                'token' => $newToken
            ]
        ]);
    }

    /**
     * Get authenticated user.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUser()
    {
        return new JsonResponse([
            'message' => 'authenticated_user',
            'data' => JWTAuth::parseToken()->authenticate()
        ]);
    }

    /** 
     * Get user created or for first time Registration of user.
     *
     * @return \Illuminate\Http\Response
     */
    public function createUser(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'contact_no' => 'required',
            'city' => 'required',
        ]);
        $requestedNewUser = new User;
        $requestedNewUser->name = $request->input('name');
        $requestedNewUser->email = $request->input('email');
        $requestedNewUser->password = Hash::make($request->input('password'));  //created_at;
        $requestedNewUser->remember_token = uniqid();
        $requestedNewUser->created_at = date('Y-m-d H:i:s');
        $requestedNewUser->save();
        $insertedId =  $requestedNewUser->id;
        if ($insertedId) {
            $requestedNewUserDetails = new Users_detail;
            $requestedNewUserDetails->user_id = $insertedId;
            $requestedNewUserDetails->dob = $request->input('dob');
            $requestedNewUserDetails->address = $request->input('address');
            $requestedNewUserDetails->city = $request->input('city');
            $requestedNewUserDetails->state = $request->input('state');
            $requestedNewUserDetails->country = $request->input('country');
            $requestedNewUserDetails->pin_code = $request->input('pin_code');
            $requestedNewUserDetails->contact_no = $request->input('contact_no');
            $requestedNewUserDetails->alt_contact_no = $request->input('alt_contact_no');
            $requestedNewUserDetails->last_city = $request->input('last_city');
            $requestedNewUserDetails->pref_cities = $request->input('pref_cities');
            $requestedNewUserDetails->created_at = date('Y-m-d H:i:s');
            $requestedNewUserDetails->save();
            $detailsInsertedId = $requestedNewUserDetails->id;
            if ($detailsInsertedId) {
                return new JsonResponse(['message' => 'Voila! You account is successfully created.']);
            }
            else {
                return new JsonResponse(['message' => 'something went wrong with User Details Registration']);
            }
        }
        else {
            return new JsonResponse(['message' => 'something went wrong with User Registration']);
        }
    }
    /** 
     * Update the user based on user id.
     *
     * @return \Illuminate\Http\Response
     */
    public function userUpdate(Request $request, $id)
    {
        $requestedUser = User::where('id','=',$id)->first()->update($request->all());
        $requestedUserDetails = Users_detail::where('user_id','=',$id)->first()->update($request->all());
        if ($requestedUser && $requestedUserDetails) {
            return new JsonResponse(['message' => 'user_updated']);
        }
        else {
            return new JsonResponse(['message' => 'error']);
        }
    }
}
