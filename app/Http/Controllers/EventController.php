<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Lumen\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\DB;
use App\Event;
use App\Event_detail;
use App\Event_social;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'latitude' => 'required',
            'longitude' => 'required',
        ]);
        $lat = $request->input('latitude');
        $lng = $request->input('longitude');
        $distance = $request->input('distance')?: '5000';
        $eventsInArea = Event::eventsWithinDistance($lat, $lng, $distance);
        if(empty($eventsInArea)) {
            return new JsonResponse(['message' => 'no_event']);
        }
        $eventIds = [];
        foreach($eventsInArea as $event)
        {
            array_push($eventIds, $event->id);
        }
        $eventsFound = Event::where('is_active','=','1')->whereIn('id', $eventIds)->get();
        if ($eventsFound) {
            return new JsonResponse([
                'message' => 'success',
                'event_count' => count($eventsFound),
                'data' => $eventsFound,
            ]);
        }
        else {
            return new JsonResponse(['message' => 'no_active_event']);
        }
    }

 
    /**
     * Return list of all active events.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllEvents()
    {
        $allEvents = Event::orderBy('created_at', 'desc')->where('is_active','=','1')->get();
        return new JsonResponse([
            'message' => 'all_active_events',
            'data' => $allEvents,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'organizer_id' => 'required',
            'name' => 'required',
            'event_date' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'city' => 'required',
        ]);
        $newEvent = new Event;
        $newEvent->organizer_id = $request->input('organizer_id');
        $newEvent->title = $request->input('title');
        $newEvent->name = $request->input('name');
        $newEvent->details = $request->input('details');
        $newEvent->event_date = $request->input('event_date');
        $newEvent->latitude = $request->input('latitude');
        $newEvent->longitude = $request->input('longitude');
        $newEvent->city = $request->input('city');
        $newEvent->created_by = $request->input('user_id');
        $newEvent->created_at = date('Y-m-d H:i:s');
        $newEvent->save();
        $newEventId =  $newEvent->id;
        if ($newEventId) {
            $newEventDetails = new Event_detail;
            $newEventDetails->event_id = $newEventId;
            $newEventDetails->other_details = $request->input('other_details');
            $newEventDetails->address = $request->input('address');
            $newEventDetails->landmark = $request->input('landmark');
            $newEventDetails->city = $request->input('city');
            $newEventDetails->state = $request->input('state');
            $newEventDetails->country = $request->input('country');
            $newEventDetails->pin_code = $request->input('pin_code');
            $newEventDetails->primary_contact = $request->input('primary_contact');
            $newEventDetails->secondary_contact = $request->input('secondary_contact');
            $newEventDetails->email = $request->input('email');
            $newEventDetails->expected_visitors = $request->input('expected_visitors');
            $newEventDetails->main_courses = $request->input('main_courses');
            $newEventDetails->instructions = $request->input('instructions');
            $newEventDetails->images = $request->input('images');
            $newEventDetails->created_at = date('Y-m-d H:i:s');
            $newEventDetails->save();
            $eventDetailsId = $newEventDetails->id;
            if ($eventDetailsId) {
                return new JsonResponse(['message' => 'event_created']);
            }
            else {
                return new JsonResponse(['message' => 'something went wrong with Event Details creation']);
            }
        }
        else {
            return new JsonResponse(['message' => 'something went wrong with Event creation']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $requestedEventData = Event::find($id);
        $requestedEventDetailsData = Event_detail::where('event_id',$id)->get();
        if ($requestedEventData) {
            return new JsonResponse([
            'message' => 'success',
            'data' => [
                'event' => $requestedEventData,
                'event_details' => $requestedEventDetailsData,
            ]
            ]);
        }
        else {
            return new JsonResponse(['message' => 'error']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestedEvent = Event::findOrFail($id)->first()->update($request->all());
        $requestedEventDetails = Event_detail::where('event_id','=',$id)->first()->update($request->all());
        if ($requestedEvent && $requestedEventDetails) {
            return new JsonResponse(['message' => 'event_updated']);
        }
        else {
            return new JsonResponse(['message' => 'error']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $requestedEvent = Event::where('id',$id)->delete();
        $requestedEventDetails = Event_detail::where('event_id',$id)->delete();
        if ($requestedEvent && $requestedEventDetails) {
            return new JsonResponse(['message' => 'event_deleted']);
        }
        else {
            return new JsonResponse(['message' => 'error']);
        }
    }

    /**
     * Request of likes, comments and other social activity.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eventSocials(Request $request)
    {
        $this->validate($request, [
            'event_id' => 'required',
            'user_id' => 'required',
        ]);
        $event_id = $request->input('event_id');
        $user_id = $request->input('user_id');
        $eventSocialFoundOrNot = Event_social::where('event_id',$event_id)->where('user_id', $user_id)->first();
        if ($eventSocialFoundOrNot) {
            $eventSocialUpdated = $eventSocialFoundOrNot->update($request->all());
            if ($eventSocialUpdated) {
                return new JsonResponse(['message' => 'successfully_updated', 'updated_id' => $eventSocialFoundOrNot->id]);
            }
            else {
                return new JsonResponse(['message' => 'updation_error']);
            }
        }
        else {
            $newEventSocial = new Event_social;
            $newEventSocial->event_id = $request->input('event_id');
            $newEventSocial->user_id = $request->input('user_id');
            $newEventSocial->like = $request->input('like');
            $newEventSocial->dislike = $request->input('dislike');
            $newEventSocial->block = $request->input('block');
            $newEventSocial->comment = $request->input('comment');
            $newEventSocial->share = $request->input('share');
            $newEventSocial->intrested = $request->input('intrested');
            $newEventSocial->rating = $request->input('rating');
            $newEventSocial->created_at = date('Y-m-d H:i:s');
            $newEventSocial->save();
            $newEventSocialId =  $newEventSocial->id;
            if ($newEventSocialId) {
                return new JsonResponse(['message' => 'successfully_inserted', 'inserted_id' => $newEventSocialId]);
            }
            else {
                return new JsonResponse(['message' => 'insertion_error']);
            }
        }
    }
}
