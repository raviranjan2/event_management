<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event_detail extends Model
{
    protected $fillable = array('event_id', 'other_details', 'address', 'landmark', 'city', 'state', 'country', 'pincode', 'primary_contact', 'secondary_contact', 'email', 'expected_visitors', 'main_courses', 'instructions', 'images');
}
