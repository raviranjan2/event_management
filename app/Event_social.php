<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event_social extends Model
{
    protected $fillable = array('event_id', 'user_id', 'like', 'dislike', 'block', 'comment', 'share', 'intrested', 'rating');
}
