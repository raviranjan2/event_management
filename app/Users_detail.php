<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_detail extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'dob',
        'address',
        'city',
        'state',
        'country',
        'pin_code',
        'contact_no',
        'alt_contact_no',
        'last_city',
        'pref_cities',
    ];

}
