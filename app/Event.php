<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Event extends Model
{
    // Defining all the fillable, for easy updation. 
    protected $fillable = array('organizer_id', 'title', 'name', 'details', 'event_date', 'latitude', 'longitude', 'city', 'is_active', 'created_by');

    // Return Id for all events that happening particular range based on Lat and Long. 
    public static function eventsWithinDistance($lat, $lng, $distance)
    {
    $results = DB::select(DB::raw('SELECT id, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $lng . ') ) + sin( radians(' . $lat .') ) * sin( radians(latitude) ) ) ) AS distance FROM events HAVING distance < ' . $distance . ' ORDER BY distance') );
    return $results;
    }
}
