<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$api = $app->make(Dingo\Api\Routing\Router::class);

$api->version('v1', function ($api) {
    $api->post('/auth/login', [
        'as' => 'api.auth.login',
        'uses' => 'App\Http\Controllers\Auth\AuthController@postLogin',
    ]);
    $api->post('/register', [
        'as' => 'api.auth.login',
        'uses' => 'App\Http\Controllers\Auth\AuthController@createUser',
    ]);

    $api->group([
        'middleware' => 'api.auth',
    ], function ($api) {
        $api->get('/index', [
            'uses' => 'App\Http\Controllers\APIController@getIndex',
            'as' => 'api.index'
        ]);
        //To get user details using token of that user
        $api->get('/auth/user', [
            'uses' => 'App\Http\Controllers\Auth\AuthController@getUser',
            'as' => 'api.auth.user'
        ]);
        $api->post('/auth/user/update/{id}', [
            'uses' => 'App\Http\Controllers\Auth\AuthController@userUpdate',
            'as' => 'api.auth.user'
        ]);
        $api->get('/location', [
            'uses' => 'App\Http\Controllers\LocationController@index',
            'as' => 'api.auth.user'
        ]);
        $api->patch('/auth/refresh', [
            'uses' => 'App\Http\Controllers\Auth\AuthController@patchRefresh',
            'as' => 'api.auth.refresh'
        ]);
        $api->delete('/auth/invalidate', [
            'uses' => 'App\Http\Controllers\Auth\AuthController@deleteInvalidate',
            'as' => 'api.auth.invalidate'
        ]);

        $api->post('/events', [
            'uses' => 'App\Http\Controllers\EventController@index',
            'as' => 'api.events'
        ]);
        $api->post('/events/all', [
            'uses' => 'App\Http\Controllers\EventController@getAllEvents',
            'as' => 'api.events'
        ]);
        $api->post('/event/create', [
            'uses' => 'App\Http\Controllers\EventController@create',
            'as' => 'api.event.create'
        ]);
        $api->put('/event/update/{id}', [
            'uses' => 'App\Http\Controllers\EventController@update',
            'as' => 'api.event.update'
        ]);
        $api->get('/event/{id}', [
            'uses' => 'App\Http\Controllers\EventController@show',
            'as' => 'api.event.view'
        ]);
        $api->delete('/event/delete/{id}', [
            'uses' => 'App\Http\Controllers\EventController@destroy',
            'as' => 'api.event.delete'
        ]);
        $api->post('/event/social', [
            'uses' => 'App\Http\Controllers\EventController@eventSocials',
            'as' => 'api.event.social'
        ]);
    });
});
