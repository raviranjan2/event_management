-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 27, 2019 at 12:59 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dara`
--

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(100) NOT NULL,
  `organizer_id` int(100) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `details` longtext,
  `event_date` datetime NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `city` varchar(100) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `organizer_id`, `title`, `name`, `details`, `event_date`, `latitude`, `longitude`, `city`, `is_active`, `created_by`, `created_at`, `updated_at`) VALUES
(2, 2, 'Test Event title 221', 'Second Event2', 'This is Second event details', '2019-02-21 13:31:25', '28.634253', '77.291600', 'East Delhi', 1, '2', '2019-02-19 15:01:40', '2019-02-22 13:03:10'),
(3, 2, 'Test Event title 2', 'Second Event', 'This is Second event details', '2019-02-20 13:31:25', '28.634253', '77.291600', 'East Delhi', 1, '3', '2019-02-20 09:37:24', '2019-02-20 09:37:24'),
(4, 20, 'quasi', 'Mohammed Raynor', 'Asperiores est sit cum reprehenderit tenetur. Accusamus amet nihil non ut blanditiis aut nesciunt. Provident qui et officia cupiditate minus occaecati pariatur magnam.', '1988-04-01 00:24:21', '-75.818466', '-82.066364', 'Riceport', 1, '9', '1995-06-21 13:00:04', '2019-02-25 09:30:51'),
(5, 28, 'blanditiis', 'Prof. Bryana Denesik I', 'Et placeat exercitationem eius praesentium. Consequatur odit reiciendis in reiciendis. Distinctio ut aliquid officiis enim beatae excepturi omnis.', '1992-04-07 21:15:50', '89.027198', '166.978637', 'Marvinfurt', 1, '5', '1990-08-02 05:16:30', '2019-02-25 09:30:51'),
(6, 40, 'unde', 'Lee Funk', 'Dolores nesciunt itaque quisquam qui. Est et dolores tempore enim sed. Architecto esse expedita voluptas vero ea reprehenderit.', '2006-05-14 10:50:37', '-5.472392', '32.371514', 'Kesslerhaven', 1, '3', '1998-04-24 18:45:58', '2019-02-25 09:30:51'),
(7, 5, 'doloremque', 'Maximus Homenick', 'Perspiciatis mollitia iure tempora aut ratione. Quasi voluptatem cumque consequatur eum occaecati modi aliquid. In minima temporibus ullam omnis. Ut voluptatem natus ab eum. Necessitatibus qui sit asperiores voluptates nisi quis voluptate.', '1980-08-08 14:47:27', '-61.748302', '48.417836', 'East Marisaview', 1, '3', '1991-08-28 08:37:43', '2019-02-25 09:30:51'),
(8, 31, 'eos', 'Ettie Lakin III', 'Omnis iusto quod et voluptas ducimus sunt sunt minus. Odio expedita vel ex dolores mollitia. Et a qui in qui aut harum.', '1973-02-22 15:18:59', '-4.665578', '-77.04202', 'Jettiemouth', 1, '8', '2001-07-16 10:37:25', '2019-02-25 09:30:51'),
(9, 22, 'ipsam', 'Mrs. Rosalyn Steuber V', 'Dolorem repudiandae doloribus voluptatem aut sint voluptatum quo. Et necessitatibus nam delectus corrupti error rem et. Sed impedit repudiandae delectus quas debitis rerum. Ut odit amet mollitia omnis porro harum.', '2010-03-12 03:36:48', '19.971952', '112.22826', 'Raoulview', 1, '10', '2003-07-30 08:12:54', '2019-02-25 09:30:51'),
(10, 14, 'eius', 'Arnaldo Kub', 'Et et enim rerum unde. Sunt blanditiis in cupiditate voluptatem. Beatae qui est voluptatum aut sint debitis.', '2000-08-16 19:11:14', '24.397861', '-63.200297', 'Shieldsberg', 1, '5', '1998-01-24 15:51:38', '2019-02-25 09:30:51'),
(11, 12, 'nostrum', 'Elwin Emmerich', 'Deleniti doloremque dolorem consequatur accusamus ipsam est sed. Rerum sed eum reprehenderit et ut consequatur. Ipsum vitae hic earum laborum. Nihil iure maxime impedit impedit consequatur nobis.', '2006-12-12 11:28:46', '-17.090814', '-141.674201', 'Nolanfurt', 1, '5', '2003-08-22 00:10:03', '2019-02-25 09:30:51'),
(12, 5, 'ut', 'Delta Gibson', 'Non quam ducimus ullam tempora aut. Sit est quia et saepe assumenda reprehenderit. Eos et totam eum est. Iusto nulla ipsam quos et ab.', '2016-04-05 10:03:00', '-56.461592', '46.643098', 'West Brodyberg', 1, '2', '2018-11-30 19:04:26', '2019-02-25 09:30:51'),
(13, 27, 'quia', 'Bertrand Von', 'Unde quam optio quia laboriosam. Ullam quis perspiciatis ut doloribus quos deleniti nulla. Rerum veritatis odit odit consequatur facilis iure voluptas. Reprehenderit quisquam et dolorem quia qui qui.', '1991-05-26 03:06:03', '-45.200704', '75.909593', 'Felipeton', 1, '6', '1988-10-19 23:49:12', '2019-02-25 09:30:51'),
(14, 11, 'alias', 'Tre Robel IV', 'Libero dolor nisi molestias vitae voluptas velit. Quas quidem reprehenderit numquam necessitatibus minima. Aperiam nulla qui illo non. Rem sunt quidem voluptas magni. Sunt nihil nisi dolore tempora sed omnis deserunt.', '2009-10-07 19:37:59', '75.29641', '-159.225167', 'New Savanahstad', 1, '1', '1983-12-18 08:30:29', '2019-02-25 09:30:51'),
(15, 13, 'et', 'Dr. Maryjane Roob V', 'Unde magni enim veritatis velit totam eos nam nam. Sint doloremque et ut enim quasi. Sed ut nisi sequi aut ut minima. Placeat similique corporis quis eum totam alias veniam.', '1989-08-14 15:16:40', '50.179405', '-5.134084', 'South Sabrinatown', 1, '1', '1990-02-14 16:17:12', '2019-02-25 09:30:52'),
(16, 31, 'natus', 'Ms. Brittany Walsh IV', 'Fugit libero velit dicta. Quod maxime impedit sit explicabo consequatur illum. Autem possimus est veniam odio cum nemo.', '2014-05-17 16:14:16', '58.81353', '70.233335', 'Zulaufshire', 1, '2', '2017-11-17 14:03:47', '2019-02-25 09:30:52'),
(17, 26, 'sunt', 'Jaycee Dickens', 'Perferendis dolor porro impedit et impedit. Asperiores nobis reprehenderit cumque. Non repellendus qui rerum.', '2008-10-24 09:23:22', '-65.838103', '87.073743', 'South Websterville', 1, '6', '1977-01-28 19:39:50', '2019-02-25 09:30:52'),
(18, 16, 'sapiente', 'Hortense Thompson', 'Exercitationem dolorum architecto quaerat laudantium. Nihil ratione minus qui nulla atque perferendis. Doloribus repudiandae accusamus omnis dolor voluptas et iste.', '1994-12-01 03:54:51', '-63.322162', '142.41601', 'Port Jena', 1, '8', '1980-10-07 12:43:27', '2019-02-25 09:30:52'),
(19, 13, 'at', 'Prof. Helga Gerlach', 'Placeat quibusdam doloribus ratione qui est non totam. Illum corrupti aut non libero ex. Quidem maxime quaerat nobis voluptatem eligendi explicabo alias. Dolores hic minus quod qui voluptatibus voluptas illum.', '2009-09-19 04:01:01', '36.816613', '173.408321', 'Millerstad', 1, '5', '1992-02-29 14:49:42', '2019-02-25 09:30:52'),
(20, 40, 'quibusdam', 'Ada Cole', 'Quasi molestiae architecto ipsum magnam vel unde sapiente suscipit. Blanditiis qui voluptas quod sint hic. Harum expedita molestiae fugit.', '2011-05-21 07:32:17', '-61.379673', '-176.230436', 'New Mose', 1, '5', '2004-11-06 04:47:52', '2019-02-25 09:30:52'),
(21, 19, 'eos', 'Kaci Johnson', 'Impedit totam optio voluptatem pariatur. Architecto enim mollitia rerum illum modi. Molestiae quo nam qui iure qui non.', '2004-05-05 12:01:09', '-31.878274', '-110.500211', 'North Websterberg', 1, '2', '1979-12-10 02:37:27', '2019-02-25 09:30:52'),
(22, 14, 'et', 'Kale Considine', 'Id ea sint quia est et similique. Debitis repellat harum recusandae consectetur. Recusandae dolores non non veritatis.', '1992-09-12 10:56:24', '-52.810125', '142.367585', 'West Iva', 1, '1', '1979-12-25 22:31:01', '2019-02-25 09:30:52'),
(23, 14, 'natus', 'Lera Corwin', 'Praesentium sapiente ut quos iusto tempora dicta dolore. Magnam non enim sunt autem dolores ratione suscipit. Officiis exercitationem eum omnis repellendus eos labore quo.', '2004-11-04 07:28:23', '10.02569', '-139.94885', 'Rogersmouth', 1, '2', '1992-07-18 01:28:11', '2019-02-25 09:30:52');

-- --------------------------------------------------------

--
-- Table structure for table `event_details`
--

CREATE TABLE `event_details` (
  `id` int(100) NOT NULL,
  `event_id` int(100) NOT NULL,
  `other_details` varchar(250) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `landmark` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `pin_code` varchar(20) DEFAULT NULL,
  `primary_contact` varchar(20) DEFAULT NULL,
  `secondary_contact` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `expected_visitors` int(50) DEFAULT NULL,
  `main_courses` varchar(250) DEFAULT NULL,
  `instructions` varchar(250) DEFAULT NULL,
  `images` varchar(250) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event_details`
--

INSERT INTO `event_details` (`id`, `event_id`, `other_details`, `address`, `landmark`, `city`, `state`, `country`, `pin_code`, `primary_contact`, `secondary_contact`, `email`, `expected_visitors`, `main_courses`, `instructions`, `images`, `created_at`, `updated_at`) VALUES
(1, 2, 'Test other details for first event', 'Street 1', 'Near HDFC Bank ATM', 'East Delhi', 'Delhi', 'India', '110092', '90000001', '90000011', 'helloravi@live.com', 100, 'Test Main course 1', 'Test instructions for event 1', 'http://localhost/images/image1.png', '2019-02-19 15:01:40', '2019-02-19 15:01:40'),
(2, 3, 'Test other details for Second event', 'Street 222', 'Near ICICI Bank ATM', 'East Delhi', 'Delhi', 'India', '110092', '90000002', '90000022', 'helloravi@live.com', 150, 'Test Main course 2', 'Test instructions for event 2', 'http://localhost/images/image2.png', '2019-02-20 09:37:24', '2019-02-22 13:31:30'),
(3, 9, 'Adipisci commodi officia ut alias ipsa aut quod. Laboriosam ipsa quasi ut quos fuga omnis a. Dolorem et dolorem harum quia maiores et. Quae ipsam est esse sit. Corrupti iusto aut facilis quod qui vero.', '40590 Kertzmann Manor Apt. 851\nKilbackfort, MD 81495-8131', 'Isabell Camp', 'Ovaton', 'Hawaii', 'Iraq', '59457', '435.372.2100 x044', '+1-318-956-2899', 'cecelia13@yahoo.com', 197, 'Rem voluptatem quam ducimus rem in laudantium aut. Adipisci ex error ut quia sit accusamus illum. Mollitia molestias consequatur voluptatem molestiae et ut.', 'Mollitia velit aut qui et. Earum voluptatem ipsa quisquam sit rerum voluptatem aut. Ea ipsa consequatur voluptas. Beatae beatae autem rerum eveniet velit debitis similique.', 'http://lorempixel.com/640/480/?64283', '2003-10-22 08:58:16', '2019-02-25 09:30:51'),
(4, 10, 'Est et rerum suscipit aliquid. Sed vel et ut expedita.', '8649 Bruen Views Apt. 531\nReymundoberg, MO 36189-8229', 'Delilah Bridge', 'South Abdullahbury', 'Hawaii', 'Micronesia', '20619-2859', '+1-775-690-7586', '380-547-0412 x17534', 'sadye.grant@gmail.com', 48, 'Veritatis placeat autem facilis et incidunt sed velit. Accusamus incidunt reiciendis quia quis illum similique. Omnis et suscipit quaerat debitis. Ut provident quaerat quia nemo enim fugit.', 'Iste maiores et est non omnis. Voluptas excepturi suscipit iste ullam sit est aliquam. Nesciunt dolorem sit velit sed consequatur atque nobis.', 'http://lorempixel.com/640/480/?56355', '1983-09-23 18:30:39', '2019-02-25 09:30:51'),
(5, 8, 'Fugit architecto minus voluptas cumque praesentium. Deserunt quisquam magni explicabo. Voluptatibus eum et mollitia ab.', '179 Mueller Motorway Apt. 903\nNorth Moses, CA 56928-9498', 'Cyril Mountains', 'East Marlenemouth', 'Ohio', 'Bolivia', '31033-7147', '(785) 691-4691 x2732', '1-684-355-6688 x351', 'connelly.jonatan@hintz.com', 128, 'Harum et aliquid labore aliquid nihil earum ipsa eveniet. Quisquam et distinctio et quas. Sint et laboriosam sed quod.', 'Neque et molestiae quidem officia rerum dolorem dolor. Qui doloremque nobis quia quam error minus. Modi repellat qui corrupti dolorum qui.', 'http://lorempixel.com/640/480/?42739', '2009-12-23 07:27:50', '2019-02-25 09:30:51'),
(6, 5, 'Sint blanditiis voluptatem omnis est perspiciatis nemo voluptas. Eos qui distinctio in placeat cum in rerum. Similique repellat voluptatem molestiae rerum tenetur voluptate omnis. Ullam animi voluptatum eaque sunt.', '22515 Kuhn Mount Apt. 520\nKihnland, PA 63770-8405', 'Gleichner Station', 'East Lonnychester', 'Hawaii', 'Morocco', '55298-3738', '635.246.5689 x2917', '1-848-888-4584 x372', 'shany44@gmail.com', 147, 'Amet similique quo corrupti quia id. Quia natus omnis velit similique necessitatibus est. Explicabo facere consequatur et quasi expedita in.', 'Sit suscipit sed rem rerum blanditiis neque ullam. Deserunt rerum sit ut placeat. Maxime sed nostrum sed.', 'http://lorempixel.com/640/480/?18512', '1993-05-09 20:35:53', '2019-02-25 09:30:51'),
(7, 4, 'Officiis voluptates quia omnis dolor voluptates et. Enim itaque ab eos ut est. Harum et voluptatum veniam et qui eligendi nihil. Minima cumque nihil ut minima ipsa.', '5240 Sonny Tunnel\nZiemannfurt, KS 91784-4024', 'Deckow Circle', 'New Michelburgh', 'North Carolina', 'Heard Island and McDonald Islands', '15977-9448', '1-252-904-9827 x990', '+1 (405) 693-6476', 'mgoodwin@yahoo.com', 97, 'Sed eius sint quod voluptate temporibus dolorem non voluptatem. Aut fugiat veritatis dolore dolorem. Saepe optio debitis sed tenetur nisi et sunt.', 'Quo ad hic quis quam labore. Itaque adipisci ducimus blanditiis dolores officiis voluptatem.', 'http://lorempixel.com/640/480/?32668', '1984-05-15 12:04:12', '2019-02-25 09:30:51'),
(8, 1, 'Earum et numquam sint nihil quasi. Quisquam laudantium sed consequatur quibusdam. Ex sed qui sed sit explicabo autem consequatur.', '11447 Tremblay Branch\nFeestburgh, GA 83738-7352', 'Jast Parkway', 'Mayamouth', 'Tennessee', 'Czech Republic', '40460', '(743) 331-8218 x4785', '921.526.9116', 'rosendo86@yahoo.com', 38, 'Ducimus incidunt molestiae repellendus repudiandae. Deleniti ea facere ipsa molestiae voluptatem. Qui temporibus eum sed ut inventore. Sequi laboriosam rerum ad voluptas.', 'Corrupti deleniti quasi consectetur ut sit. Ab ipsam velit quisquam nemo accusamus quae consequatur.', 'http://lorempixel.com/640/480/?63468', '2014-11-11 10:22:50', '2019-02-25 09:30:51'),
(9, 2, 'Repudiandae illo molestiae maxime consequuntur voluptatem laboriosam. Aspernatur a velit et et voluptatum et voluptas.', '1400 O\'Kon Stream\nPort Victor, WV 32751', 'Jakubowski Plain', 'Krisfurt', 'Virginia', 'Suriname', '29745', '1-291-470-6116', '1-362-368-8180 x545', 'vfeil@yahoo.com', 196, 'Aliquid quo modi tenetur voluptatum culpa. Quis fuga exercitationem iusto qui ut esse cum quibusdam. Reiciendis maiores qui consequatur quia.', 'Voluptas commodi voluptatem numquam sed suscipit fugit libero. Illum totam facilis occaecati. Ullam cupiditate voluptates nostrum.', 'http://lorempixel.com/640/480/?48953', '1986-10-06 02:24:06', '2019-02-25 09:30:51'),
(10, 3, 'Autem ut architecto optio. Maxime fugiat consequatur hic cumque tenetur aut consequatur. Perspiciatis in sit ut debitis at. Accusamus nisi sit consequatur voluptas ut cumque. Quia minima rerum provident.', '51750 Deckow Overpass\nEsmeraldatown, SD 16713-4712', 'Roger Glen', 'New Candido', 'North Carolina', 'Saint Helena', '42246-1078', '860.526.6275 x7586', '1-357-390-9571 x6049', 'ubernier@cole.biz', 133, 'Itaque placeat vitae tempore et sunt enim praesentium. Ut placeat delectus voluptas sunt id aut. Quia molestiae occaecati voluptate et.', 'Sit sapiente et ipsa rerum ut doloremque culpa. Vero reiciendis ad in magnam.', 'http://lorempixel.com/640/480/?18659', '1979-07-06 13:46:56', '2019-02-25 09:30:51'),
(11, 6, 'Est vel laborum consequuntur enim. Quia sed distinctio maxime doloremque temporibus voluptatum ea. Consequuntur minima repellendus quos culpa eveniet libero sapiente. Perferendis et possimus voluptates molestias voluptate quasi enim. Nesciunt consequ', '79207 Bernhard Mission\nEast Malika, NY 09215-6201', 'Bridget Lake', 'Roderickview', 'Vermont', 'Guam', '52651', '507.470.6577', '+1.784.485.6345', 'verner14@zulauf.info', 151, 'Sed at qui quibusdam dicta. Tenetur nihil ipsam fuga accusamus iste iste quasi. Molestiae illum sit laborum. Sit quo culpa qui quaerat dolorem.', 'Facilis velit quibusdam accusamus. Aut enim illo qui nemo tenetur blanditiis quas. Aut nemo odio perspiciatis ab quis.', 'http://lorempixel.com/640/480/?19572', '2002-06-20 23:58:00', '2019-02-25 09:30:51'),
(12, 10, 'Ducimus in sit ut dolores et pariatur ut temporibus. Dolorem illum qui odio. Esse cupiditate voluptatum facilis fuga incidunt. Voluptatem voluptas rem accusantium omnis. Neque qui sunt suscipit corporis.', '2762 Ward Pike Apt. 484\nWest Cierra, ND 73304-6470', 'Hackett Highway', 'Lake Judsontown', 'Wyoming', 'Botswana', '45576-3374', '+1.210.741.9851', '+1-765-571-9745', 'esenger@turcotte.com', 199, 'Molestiae consectetur molestiae tempore in. At velit nobis alias est est inventore.', 'Ratione sit sit ratione harum. Reprehenderit illo ad corporis cum eligendi ex. Vitae et in dicta consequatur magni est iste. Quaerat doloremque quibusdam est sint et.', 'http://lorempixel.com/640/480/?89486', '2003-04-05 09:06:09', '2019-02-25 09:30:51'),
(13, 7, 'Quis enim laudantium placeat enim reiciendis. Nulla cum officiis rerum optio velit hic totam. Eos voluptas ea omnis tempore. Enim pariatur et et.', '1959 Rohan Shores\nNorth Wendychester, CA 88272-2951', 'Izaiah Shore', 'Melanymouth', 'Wisconsin', 'Ireland', '94968', '1-918-917-0942 x1529', '1-386-310-3909 x200', 'ruecker.marilou@hotmail.com', 43, 'Eos architecto voluptas adipisci eligendi. Vel maxime numquam maiores molestiae tempora quia. Vel mollitia non ut illum laudantium beatae itaque.', 'Dolores laborum aut ut non quam inventore id. Perspiciatis sunt asperiores at impedit aliquid. Nobis sed dignissimos et cupiditate voluptatibus.', 'http://lorempixel.com/640/480/?59252', '1991-12-07 18:31:53', '2019-02-25 09:30:51'),
(14, 4, 'Fugit quasi ullam omnis reprehenderit. Sit incidunt cumque et veniam aperiam illum. Velit aut dicta tempore modi placeat pariatur assumenda.', '505 Estella Station\nFritschview, CO 83008-4994', 'Hayes Park', 'Connellyburgh', 'Kentucky', 'Angola', '61837', '231-542-7140 x7020', '1-887-805-7757', 'nicolette.koepp@ondricka.net', 48, 'Eum unde aliquid quia officia ex beatae dicta. Commodi quo nobis ut aperiam facere voluptatem. Autem quod facere quidem. Quas voluptas veniam atque cum.', 'Laborum recusandae autem qui ipsam. Quidem ratione distinctio ut. Deleniti magnam earum nihil sit harum explicabo doloribus.', 'http://lorempixel.com/640/480/?16508', '2000-07-26 06:31:58', '2019-02-25 09:30:52'),
(15, 3, 'Explicabo accusamus qui quia aut recusandae est id. Sunt exercitationem cumque exercitationem tempora et eum deleniti. Voluptas alias architecto vero in ea incidunt. Repudiandae quia optio facere non sunt.', '3549 Geovany Pike\nSouth Athena, VT 27661', 'Hermann Bypass', 'Port Deontae', 'Illinois', 'Libyan Arab Jamahiriya', '98902', '1-387-235-6113 x0348', '572-309-0504 x515', 'ubergnaum@stracke.com', 137, 'Ab et rem voluptatem in. Adipisci reprehenderit sed aliquam perspiciatis possimus quos tempora. Totam ad ea et minus placeat ut ducimus.', 'Vitae officia rem voluptatem velit nostrum. Qui in quo sed voluptatem veritatis odit. Debitis dignissimos et unde quo eos dolores unde.', 'http://lorempixel.com/640/480/?91792', '1997-04-11 08:45:15', '2019-02-25 09:30:52'),
(16, 3, 'Fuga delectus quisquam quos excepturi hic. Ipsa cumque odit rerum et qui facere. Non praesentium veritatis quisquam ea voluptatum. Est aut rem suscipit dicta.', '79119 Nienow Corners Apt. 040\nBonnieshire, SD 99429', 'Will Neck', 'New Vitoland', 'Arkansas', 'Cocos (Keeling) Islands', '74777', '(591) 262-2062 x479', '370.583.9897', 'tate.wuckert@goyette.com', 173, 'Ex consequuntur similique laboriosam aliquam odit non et. Fuga ut unde voluptas eos. Autem molestiae et id voluptatem voluptatum fuga.', 'Ducimus assumenda molestias aliquam est sunt. Dolor modi autem facilis magni facilis molestias porro. Ut quibusdam molestiae quibusdam consectetur minus explicabo.', 'http://lorempixel.com/640/480/?56833', '1987-03-29 12:35:09', '2019-02-25 09:30:52'),
(17, 9, 'Enim iste sint ullam officiis doloremque nulla. Itaque vel ut culpa ullam fugit totam. Omnis aut et architecto itaque minima.', '1204 Kassulke Creek Suite 310\nSouth Jamiefurt, HI 00754', 'Grimes Parkways', 'Port Felicialand', 'Mississippi', 'Lithuania', '28809-2272', '(696) 949-1727 x2460', '+15689743229', 'patsy.bartell@gmail.com', 78, 'Voluptas totam neque explicabo. Ut perspiciatis perspiciatis consequatur nemo ducimus. Maiores ipsam velit eum est rerum minus illum.', 'Est excepturi voluptas est illo. Natus officiis est omnis repellat dolor id ea velit. Ratione accusamus commodi aut.', 'http://lorempixel.com/640/480/?37851', '1984-09-06 06:57:44', '2019-02-25 09:30:52'),
(18, 2, 'Ad laudantium soluta tenetur illo doloremque accusamus ut dolores. Aliquid voluptatem adipisci rerum sed minima. Ab qui quas quis enim est. Ipsa modi porro eum dolore voluptas eos aliquid. Voluptatem vitae et excepturi ipsam.', '771 Leannon Crest Suite 022\nKathryneton, CO 93394-0054', 'Terry Locks', 'Mortimerville', 'Maryland', 'Rwanda', '61426-8339', '(856) 553-3616 x1728', '1-453-994-9096', 'elmore77@gmail.com', 160, 'Expedita eligendi libero laboriosam laboriosam odio nam veritatis ad. Architecto consequatur omnis corporis amet. Rerum ex similique tenetur sit ut et sint aut. Aut natus expedita enim.', 'Maiores eum et cumque et commodi hic maxime. Et consequatur voluptatem sed eum aut necessitatibus iste. Enim tempore omnis quaerat ut et et nihil.', 'http://lorempixel.com/640/480/?97619', '1973-02-05 08:28:57', '2019-02-25 09:30:52'),
(19, 6, 'Delectus qui ipsa omnis itaque. Repellendus nemo dolorem distinctio sint explicabo libero commodi. Sit corporis quis dolorum a ut est. Aut voluptate et accusantium earum laudantium nulla.', '10973 Emmy Alley Apt. 773\nNew Ramona, AZ 05657-9036', 'Electa Bridge', 'Lake Clarabelle', 'New York', 'French Guiana', '35093', '1-451-991-7577', '1-881-764-6222 x8474', 'batz.lilliana@heller.com', 83, 'Dolores aspernatur et qui sit sed debitis animi eum. Neque unde error aut quia nisi. Magni autem ut possimus est incidunt vero.', 'Voluptas est officiis sed rerum. Eius veritatis doloribus rerum quo. Vel praesentium consequatur veniam dolor qui et doloremque.', 'http://lorempixel.com/640/480/?50586', '1986-03-05 01:13:13', '2019-02-25 09:30:52'),
(20, 8, 'Illum ipsam nihil tenetur a nihil fugit quaerat molestiae. Distinctio natus et aut ad non quo eaque occaecati. Nihil tempora sit quibusdam quia non iste. Tempora maxime voluptatem quia sed error dolorum enim.', '800 Franecki Brooks\nFraneckimouth, WI 60667', 'Herzog Trail', 'Port Donavonstad', 'South Carolina', 'Belize', '87164', '1-647-206-1624', '1-398-843-0302', 'sgislason@yahoo.com', 103, 'Quia soluta et sit quia. Illum occaecati animi rerum. Iste et eos tempora ex nisi et. Itaque explicabo qui velit neque exercitationem commodi eius. Molestiae officia aut aperiam aut.', 'Ad tempore pariatur in. Aut deleniti quasi atque hic. In ab omnis quam odio. Molestiae vero sed et iure vel quia dolorum.', 'http://lorempixel.com/640/480/?55826', '1996-03-08 07:01:55', '2019-02-25 09:30:52'),
(21, 3, 'Autem pariatur amet ut dolorem totam possimus molestias. Facere minus sit vel aspernatur temporibus. Enim enim dolor sed non unde. Tempora voluptas ipsam sapiente asperiores suscipit. Inventore possimus vel dolores voluptatem voluptatibus mollitia.', '3720 Christiansen Port Suite 280\nKleinmouth, KS 62021-8850', 'Mattie Landing', 'Casandrafort', 'Virginia', 'Macedonia', '28479-3118', '917.399.9696', '+1-321-531-0294', 'roosevelt.gleason@gottlieb.info', 78, 'Officiis neque ut facere voluptatem. Vitae aut ipsam et quos. Deserunt explicabo eum libero nobis voluptatum ea distinctio sunt. Et atque aut quod.', 'Quia quis suscipit sunt molestias. Voluptatem non voluptatum quia porro quibusdam. Mollitia laudantium atque dolores sed voluptate inventore.', 'http://lorempixel.com/640/480/?99674', '2004-09-17 04:56:15', '2019-02-25 09:30:52'),
(22, 4, 'Quas magni sed asperiores mollitia tempore quis id. Quidem ut illo non minus aliquam. Ab qui fuga exercitationem qui nulla architecto.', '1597 Ullrich Station\nWest Macy, GA 00682', 'Hadley Drive', 'Nyasiaview', 'Ohio', 'Myanmar', '30522-4264', '+1 (806) 975-0822', '272.467.5420 x03841', 'emiliano.hauck@reynolds.net', 66, 'Iure magni voluptatibus quibusdam. Et amet quis laborum quia consequatur. Sit omnis iusto possimus sint explicabo dignissimos.', 'Ad possimus in aut totam quidem dolorem. Totam repellendus eligendi sed dolorem adipisci corporis. Ducimus eaque quam laboriosam minima ea vel autem tempore.', 'http://lorempixel.com/640/480/?68849', '2003-02-15 12:29:50', '2019-02-25 09:30:52');

-- --------------------------------------------------------

--
-- Table structure for table `event_socials`
--

CREATE TABLE `event_socials` (
  `id` int(10) UNSIGNED NOT NULL,
  `event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `like` tinyint(4) DEFAULT '0',
  `dislike` tinyint(4) DEFAULT '0',
  `block` tinyint(4) DEFAULT '0',
  `comment` mediumtext COLLATE utf8_unicode_ci,
  `share` tinyint(4) DEFAULT '0',
  `intrested` tinyint(4) DEFAULT '0',
  `rating` text COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `event_socials`
--

INSERT INTO `event_socials` (`id`, `event_id`, `user_id`, `like`, `dislike`, `block`, `comment`, `share`, `intrested`, `rating`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 0, 0, ' This is test comment ', 1, 1, ' 4.5', '2019-02-26 12:59:00', '2019-02-26 13:05:14'),
(2, 3, 3, 1, 0, 0, ' This is test comment ..', 1, 1, ' 4.5', '2019-02-26 13:18:07', '2019-02-26 13:27:30'),
(3, 3, 4, 11, 0, 0, ' This is test comment ..', 1, 1, ' 4.5', '2019-02-26 13:29:13', '2019-02-26 13:29:13');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2016_03_08_164317_create_users_table', 1),
(2, '2016_03_08_164400_create_password_resets_table', 1),
(3, '2019_02_15_113555_users_details', 2),
(4, '2019_02_26_083028_create_event_socials', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Ravi K Ranjan', 'johndoe@example.com', '$2y$10$itmPQ/f6wg9zLNALmql1P.mw7cOytXs2JU0HZHKNXB70UMBmYCFZ.', 'vhRL8ktjzv', NULL, '2019-02-26 15:01:11'),
(2, 'Ravi Ranjan', 'helloravi@live.com', '$2y$10$HHHguBriAnbBHCLoqLNsE.agVPwdJI97NlI7Lk3qHTRronk5VVsSm', NULL, '2019-02-18 13:31:25', '2019-02-18 13:31:25'),
(4, 'Ravi K Ranjan', 'user1@ve.com', '$2y$10$l7DkaNfqLHYthohAo9kxGOnwW71xVir5RK0et9wK7eGQyJSloOONa', '5c6ac91b451a0', '2019-02-18 15:02:51', '2019-02-26 15:11:43');

-- --------------------------------------------------------

--
-- Table structure for table `users_details`
--

CREATE TABLE `users_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dob` date DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `city` text COLLATE utf8_unicode_ci,
  `state` text COLLATE utf8_unicode_ci,
  `country` text COLLATE utf8_unicode_ci,
  `pin_code` int(11) DEFAULT NULL,
  `contact_no` text COLLATE utf8_unicode_ci,
  `alt_contact_no` text COLLATE utf8_unicode_ci,
  `last_city` text COLLATE utf8_unicode_ci,
  `pref_cities` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_details`
--

INSERT INTO `users_details` (`id`, `user_id`, `dob`, `address`, `city`, `state`, `country`, `pin_code`, `contact_no`, `alt_contact_no`, `last_city`, `pref_cities`, `created_at`, `updated_at`) VALUES
(1, 4, '1992-06-07', 'changedAddress..', 'City 1', 'State 1', 'Country 1', 110001, '90000001', '90000011', 'New Delhi', 'Bengluru', '2019-02-18 15:02:51', '2019-02-27 09:36:45'),
(2, 1, '1992-06-07', 'changedAddress 2', 'City 2', 'State 2', 'Country 2', 110092, '90000002', '90000022', 'New Delhi', 'Goa', '2019-02-18 15:02:51', '2019-02-27 09:36:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_details`
--
ALTER TABLE `event_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_socials`
--
ALTER TABLE `event_socials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `users_details`
--
ALTER TABLE `users_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_details_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `event_details`
--
ALTER TABLE `event_details`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `event_socials`
--
ALTER TABLE `event_socials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users_details`
--
ALTER TABLE `users_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_details`
--
ALTER TABLE `users_details`
  ADD CONSTRAINT `users_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
