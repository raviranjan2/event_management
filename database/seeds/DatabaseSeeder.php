<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call('UserTableSeeder'); 
        $faker = Faker::create();
    	foreach (range(1,20) as $index) {
	        DB::table('events')->insert([
	            'organizer_id' => $faker->numberBetween(1,40),
	            'title' => $faker->word,
	            'name' => $faker->name,
	            'details' => $faker->paragraph,
	            'event_date' => $faker->dateTime($max = 'now', $timezone = 'Asia/Kolkata'),
	            'latitude' => $faker->latitude($min = -90, $max = 90),
	            'longitude' => $faker->longitude($min = -180, $max = 180),
	            'city' => $faker->city,
	            'created_by' => $faker->numberBetween(1,10), 
	            'created_at' => $faker->dateTime($max = 'now', $timezone = 'Asia/Kolkata'),
            ]);
            DB::table('event_details')->insert([
	            'event_id' => $faker->numberBetween(1,10),
	            'other_details' => $faker->paragraph,
	            'address' => $faker->address,
	            'landmark' => $faker->streetName,
	            'city' => $faker->city,
	            'state' => $faker->state,
	            'country' => $faker->country,
                'pin_code' => $faker->postcode,
                'primary_contact' => $faker->phoneNumber,
	            'secondary_contact' => $faker->phoneNumber,
	            'email' => $faker->email,
	            'expected_visitors' => $faker->numberBetween(1,200),
	            'main_courses' => $faker->text,
	            'instructions' => $faker->text,
	            'images' => $faker->imageUrl($width = 640, $height = 480),
	            'created_at' => $faker->dateTime($max = 'now', $timezone = 'Asia/Kolkata'),
	        ]);
        }
    }
}
