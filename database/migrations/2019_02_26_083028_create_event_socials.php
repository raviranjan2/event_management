<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventSocials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_socials', function($table) {
            $table->increments('id', true);
            $table->integer('event_id');
            $table->integer('user_id');
            $table->tinyInteger('like')->default('0');
            $table->tinyInteger('dislike')->default('0');
            $table->tinyInteger('block')->default('0');
            $table->mediumText('comment');
            $table->tinyInteger('share')->default('0');
            $table->tinyInteger('intrested')->default('0');
            $table->text('rating');
            $table->dateTime('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
