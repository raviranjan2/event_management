<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('users_details', function($table) {
            $table->increments('id', true);
            $table->integer('user_id')->unsigned();
            $table->date('dob');
            $table->text('address');
            $table->text('city');
            $table->text('state');
            $table->text('country');
            $table->integer('pin_code');
            $table->text('contact_no');
            $table->text('alt_contact_no');
            $table->text('last_city');
            $table->text('pref_cities');
            //$table->datetime('created_at');
            $table->timestamps('updated_at');
        });

        Schema::table('users_details', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
